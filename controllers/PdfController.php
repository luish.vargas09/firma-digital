<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use setasign\Fpdi\Fpdi;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;

class PdfController extends Controller
{
    /**
     * Genera un PDF con un código QR y lo firma digitalmente.
     *
     * @throws \Exception Si hay algún error durante el proceso.
     * @return \yii\web\Response El archivo firmado como respuesta.
     */
    public function actionGenerate()
    {
        try {
            // Generar el código QR
            $qrCode = new QrCode('Contenido del código QR');
            $writer = new PngWriter();
            $qrCodeImage = $writer->write($qrCode)->getString();
            $qrCodePath = Yii::getAlias('@webroot/qr_code.png');
            file_put_contents($qrCodePath, $qrCodeImage);

            // Crear el PDF e insertar el código QR
            $pdf = new Fpdi();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->Cell(40, 10, 'Hello World!');
            $pdf->Image($qrCodePath, 10, 20, 50, 50); // x, y, width, height
            $pdfOutputPath = Yii::getAlias('@webroot/document_with_qr.pdf');
            $pdf->Output('F', $pdfOutputPath);

            // Firmar digitalmente el PDF
            // @TODO: Reemplazar los alias con los archivos reales
            $certificate = Yii::getAlias('@app/path/to/your/certificate.pem');
            $privateKey = Yii::getAlias('@app/path/to/your/private-key.pem');

            if (!file_exists($certificate)) {
                throw new \Exception('Certificate file not found: ' . $certificate);
            }

            if (!file_exists($privateKey)) {
                throw new \Exception('Private key file not found: ' . $privateKey);
            }

            $signedPdfPath = Yii::getAlias('@webroot/signed_document_with_qr.pdf');
            // @TODO: Reemplazar $pdfOutputPath con la ruta real del PDF
            $command = "openssl smime -sign -binary -noattr -in $pdfOutputPath -signer $certificate -inkey $privateKey -outform DER -out $signedPdfPath";
            $output = shell_exec($command);

            if (!file_exists($signedPdfPath)) {
                throw new \Exception('Failed to create signed PDF. OpenSSL output: ' . $output);
            }

            // Enviar el archivo firmado como respuesta
            return Yii::$app->response->sendFile($signedPdfPath);
        } catch (\Exception $e) {
            Yii::error($e->getMessage());
            throw $e;
        }
    }
}

