<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Generar PDF';
?>
<div class="site-generate-pdf">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <a href="<?= Url::to(['pdf/generate']) ?>" class="btn btn-primary">Generar PDF con QR y Firma Digital</a>
    </p>
</div>
