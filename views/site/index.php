<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Firma Digital';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Firmado de Electrónico</h1>
        <p class="lead">Generar PDF con QR y Firma Digital</p>
        <p>
            <a href="<?= Url::to(['pdf/generate']) ?>" class="btn btn-lg btn-success">Generar</a>
        </p>
    </div>
</div>
